/*
**  cache - OSSP Caching Library
**  Copyright (c) 2002 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002 Cable & Wireless Deutschland <http://www.cw.com/de/>
**
**  This file is part of OSSP cache, a Caching library which
**  can be found at http://www.ossp.org/pkg/cache/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  cache.h: library API
*/

#ifndef __CACHE_H__
#define __CACHE_H__

struct cache_st;
typedef cache_st cache_t;

typedef enum {
    CACHE_OK = 0,
    CACHE_ERR_INVAL,
    CACHE_ERR_SYS
} cache_rc_t;

typedef enum {
    CACHE_SET,
    CACHE_GET,
} cache_config_t;

typedef enum {
    CACHE_ST_MAXKEY,
    CACHE_CB_MUTEXACQUIRE,
    CACHE_CB_MUTEXRELEASE,
    CACHE_CB_OBJECTREMOVE,
} cache_param_t;

typedef enum {
    CACHE_INPLACE, /* re-arrange segments internally */
    CACHE_COPYOUT, /* copy as serialized object into new buffer */
    CACHE_MOVEOUT, /* move as serialized object into existing buffer */
} cache_fetch_t;

typedef struct {
    void   *ptr;
    size_t  len;
} cache_blob_t;

cache_rc_t cache_attach      (cache_t **cache, void *memptr, size_t memlen, size_t keyseg, size_t valseg);
cache_rc_t cache_config      (cache_t *cache, cache_config_t mode, cache_param_t id, ...);
cache_rc_t cache_detach      (cache_t *cache);

cache_rc_t cache_insert      (cache_t *cache, cache_blob_t key, cache_blob_t  val, time_t now, time_t expire);
cache_rc_t cache_lookup      (cache_t *cache, cache_blob_t key, cache_obj_t **obj, time_t now);
cache_rc_t cache_remove      (cache_t *cache, cache_blob_t key);

cache_rc_t cache_obj_size    (cache_obj_t *obj, size_t *size);
cache_rc_t cache_obj_fetch   (cache_obj_t *obj, cache_fetch_t mode, cache_blob_t *val);
cache_rc_t cache_obj_release (cache_obj_t *obj);

cache_rc_t cache_fetch       (cache_t *cache, cache_iter_t *iter, cache_blob_t *val);

cache_rc_t cache_iter_create (cache_t *cache, cache_iter_t *iter);
cache_rc_t cache_iter_key    (cache_t *cache, cache_iter_t *iter);
cache_rc_t cache_iter_destroy(cache_t *cache, cache_iter_t *iter);

cache_rc_t cache_export      (cache_t *cache, void *);
cache_rc_t cache_import      (cache_t *cache, void *);

#endif /* __CACHE_H__ */


/*
**  cache - OSSP Caching Library
**  Copyright (c) 2002 Ralf S. Engelschall <rse@engelschall.com>
**  Copyright (c) 2002 The OSSP Project <http://www.ossp.org/>
**  Copyright (c) 2002 Cable & Wireless Deutschland <http://www.cw.com/de/>
**
**  This file is part of OSSP cache, a Caching library which
**  can be found at http://www.ossp.org/pkg/cache/.
**
**  Permission to use, copy, modify, and distribute this software for
**  any purpose with or without fee is hereby granted, provided that
**  the above copyright notice and this permission notice appear in all
**  copies.
**
**  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
**  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
**  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
**  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
**  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
**  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
**  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
**  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
**  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
**  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
**  SUCH DAMAGE.
**
**  cache.c: library implementation
*/

/* include optional Autoconf header */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* include system API headers */
#include <stdio.h>       /* for "s[n]printf()" */

/* include own API header */
#include "cache.h"

/* boolean values */
#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE  (!FALSE)
#endif

/* forward declarations */
struct cache_st;
struct cache_obj_st;

/* type definitions */
typedef struct cache_obj_st  cache_obj_t;

/* support for relative pointers (offset pointers) */
typedef long op_t;
#define VP2OP(bp,vp) \
    (long)(((char *)(vp))-((char *)(bp)))
#define OP2VP(bp,op) \
    (void *)(((char *)(bp))+((char *)(op)))

/* definition: top-level cache structure */
struct cache_st {
    /* top level access pointers */
    op_t             opHash;        /* pointer to hash table */
    op_t             opObjMRU;      /* pointer to MRU object */
    op_t             opObjLRU;      /* pointer to LRU object */
    op_t             opObjEmpty;    /* pointer to empty object */

    /* information */
    int              bExpireReset;  /* whether expire resets on each access or is absolute */
    int              tExpireReset;  /* the optional expire reset time */

    /* sub-structure information */
    op_t             opKeyIndex;    /* base address of key bucket index */
    op_t             opKeyBuckets;  /* base address of key bucket pool */
    int              nKeyBuckets;   /* number of buckets in key bucket pool */
    op_t             opValIndex;    /* base address of key bucket index */
    op_t             opValBuckets;  /* base address of value bucket pool */
    int              nValBuckets;   /* number of buckets in value bucket pool */

    /* statistics */
    long             nInserts;
    long             nLookups;
    long             nLookupsSuccess;
    long             nRemoves;

    /* callbacks */
    /* ... */
};

/* definition: cache object */
struct cache_obj_st {
    int              bUsed;        /* used in an ongoing iteration */
    op_t             opHashNext;   /* next object in hash table collision chain */
    op_t             opQueuePrev;  /* previous object in LRU queue */
    op_t             opQueueNext;  /* next object in LRU queue */
    op_t             opKey;        /* pointer to first key segment in key index */
    size_t           nKey;         /* size of key in bytes */
    op_t             opVal;        /* pointer to first value segment in value index */
    size_t           nVal;         /* size of value in bytes */
    time_t           tExpire;      /* expire time of object */
};

